import csv
from django.core.management import BaseCommand
from django.utils import timezone

from new_door.users.models import Keyword, PDF


class Command(BaseCommand):
    help = "Loads keywords from CSV file."

    def add_arguments(self, parser):
        parser.add_argument("file_path", type=str)

    def handle(self, *args, **options):
        start_time = timezone.now()
        file_path = options["file_path"]
        with open(file_path, "r") as csv_file:
            data = csv.reader(csv_file, delimiter=",")
            pdfs = []
            for row in data:
                keyword, created = Keyword.objects.get_or_create(name=row[0])
                # if not keyword:
                #     keyword = Keyword.objects.create(name=row[0])
                pdf = PDF(
                    title=row[2],
                    link=row[1],
                    tumb='',
                    desc=row[3],
                    keyword=keyword
                )
                pdfs.append(pdf)
                if len(pdfs) > 5000:
                    PDF.objects.bulk_create(pdfs)
                    pdfs = []
            if pdfs:
                PDF.objects.bulk_create(pdfs)
        end_time = timezone.now()
        self.stdout.write(
            self.style.SUCCESS(
                f"Loading CSV took: {(end_time-start_time).total_seconds()} seconds."
            )
        )
