from django.contrib import admin
from django.contrib.auth import admin as auth_admin
from django.contrib.auth import get_user_model
from django.utils.translation import gettext_lazy as _

from new_door.users.forms import UserAdminChangeForm, UserAdminCreationForm

from new_door.users.models import Keyword, PDF, Domain, Niche, DomainGroup, ContentDB


class NicheAdmin(admin.ModelAdmin):
    list_display = ( 'id', 'name')
    list_display_links = ('name',)
    search_fields = ('name',)

class DomainGroupAdmin(admin.ModelAdmin):
    list_display = ( 'id', 'name', 'get_niche')
    list_display_links = ('name',)
    search_fields = ('name',)

    def get_niche(self, object):
        return object.niche.name

    get_niche.short_description = 'niche'


class ContentDBAdmin(admin.ModelAdmin):
    list_display = ( 'id', 'name', 'get_niche', 'imported', 'path')
    list_display_links = ('name',)
    search_fields = ('name',)
    fields = ('name', 'niche', 'file', 'imported')

    def get_niche(self, object):
        return object.niche.name

    get_niche.short_description = 'niche'


class KeywordAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')
    list_display_links = ('name',)
    search_fields = ('name',)


class PDFAdmin(admin.ModelAdmin):
    list_display = ( 'id', 'title')
    list_display_links = ('title',)
    search_fields = ('title',)


class DomainAdmin(admin.ModelAdmin):
    list_display = ( 'id', 'name')
    list_display_links = ('name',)
    search_fields = ('name',)


admin.site.register(Niche, NicheAdmin)
admin.site.register(DomainGroup, DomainGroupAdmin)
admin.site.register(ContentDB, ContentDBAdmin)
admin.site.register(Keyword, KeywordAdmin)
admin.site.register(PDF, PDFAdmin)
admin.site.register(Domain, DomainAdmin)
User = get_user_model()


@admin.register(User)
class UserAdmin(auth_admin.UserAdmin):
    form = UserAdminChangeForm
    add_form = UserAdminCreationForm
    fieldsets = (
        (None, {"fields": ("username", "password")}),
        (_("Personal info"), {"fields": ("name", "email")}),
        (
            _("Permissions"),
            {
                "fields": (
                    "is_active",
                    "is_staff",
                    "is_superuser",
                    "groups",
                    "user_permissions",
                ),
            },
        ),
        (_("Important dates"), {"fields": ("last_login", "date_joined")}),
    )
    list_display = ["username", "name", "is_superuser"]
    search_fields = ["name"]
