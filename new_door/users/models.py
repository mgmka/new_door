import datetime

from django.contrib.auth.models import AbstractUser
from django.db.models import CharField
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

from django.db import models
import os
from django.dispatch import receiver


class User(AbstractUser):
    """
    Default custom user model for New door.
    If adding fields that need to be filled at user signup,
    check forms.SignupForm and forms.SocialSignupForms accordingly.
    """

    #: First and last name do not cover name patterns around the globe
    name = CharField(_("Name of User"), blank=True, max_length=255)
    first_name = None  # type: ignore
    last_name = None  # type: ignore

    def get_absolute_url(self):
        """Get url for user's detail view.

        Returns:
            str: URL for user detail.

        """
        return reverse("users:detail", kwargs={"username": self.username})


class Niche(models.Model):
    name = models.CharField(max_length=100, verbose_name='Название нишы')

    def __str__(self):
        return self.name


class DomainGroup(models.Model):
    name = models.CharField(max_length=100)
    niche = models.ForeignKey(Niche, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Domain(models.Model):
    name = models.CharField(max_length=100, default='localhost')
    active = models.BooleanField(default=False)
    domain_group = models.ForeignKey(DomainGroup, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Keyword(models.Model):
    name = models.CharField(max_length=500)
    blacklisted = models.BooleanField(default=False)

    def __str__(self):
        return self.name


class ContentDB(models.Model):
    name = models.CharField(max_length=500)
    niche = models.ForeignKey(Niche, on_delete=models.CASCADE, null=True)
    created = models.DateTimeField(auto_now_add=True, null=True)
    file = models.FileField(default=None)
    imported = models.BooleanField(default=False)
    path = models.CharField(max_length=500, default='')

    def save(self, *args, **kwargs):
        self.path = self.file.path
        self.name = self.file.name
        super(ContentDB, self).save(*args, **kwargs)

    def __str__(self):
        return self.name


@receiver(models.signals.post_delete, sender=ContentDB)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    """
    Deletes file from filesystem
    when corresponding `MediaFile` object is deleted.
    """
    if instance.file:
        if os.path.isfile(instance.file.path):
            os.remove(instance.file.path)


@receiver(models.signals.pre_save, sender=ContentDB)
def auto_delete_file_on_change(sender, instance, **kwargs):
    """
    Deletes old file from filesystem
    when corresponding `MediaFile` object is updated
    with new file.
    """
    if not instance.pk:
        return False

    try:
        old_file = sender.objects.get(pk=instance.pk).file
    except sender.DoesNotExist:
        return False

    new_file = instance.file
    if not old_file == new_file:
        if os.path.isfile(old_file.path):
            os.remove(old_file.path)

class PDF(models.Model):
    title = models.CharField(max_length=500)
    link = models.CharField(max_length=1500)
    pdf_name = models.CharField(max_length=500, null=True)
    thumb = models.CharField(max_length=500)
    desc = models.TextField()
    text = models.TextField()
    published = models.BooleanField(default=False)
    parsed = models.BooleanField(default=False)
    keyword = models.ForeignKey(Keyword, on_delete=models.CASCADE)
    domain = models.ForeignKey(Domain, on_delete=models.CASCADE, null=True)
    content_db = models.ForeignKey(ContentDB, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.title
