from django.contrib.auth import get_user_model
from config import celery_app
from .models import Domain, PDF
import fitz
import random
import hashlib
import os
from django.conf import settings
import requests
from celery import shared_task
import time



User = get_user_model()


@shared_task
def my_task():
    time.sleep(10)
    return "ok"


@celery_app.task()
def get_users_count():
    """A pointless Celery task to demonstrate usage."""
    return User.objects.count()

@celery_app.task
def generation():
    result = []
    for domain in Domain.objects.filter(active=True):
        [result.append(generate_page.delay(domain.pk)) for _ in range(5)]
    return len(result)

@celery_app.task(bind=True)
def generate_page(self, domain):
    results = []
    domain_obj = Domain.objects.get(pk=domain)
    pdf = random.choice(PDF.objects.filter(parsed=False))
    path = os.path.join(settings.APPS_DIR, 'static/files/')
    image_path = os.path.join(settings.APPS_DIR, 'static/img/')
    sha_hash = hashlib.sha1(str(pdf.link).encode('utf-8')).hexdigest()
    file_path = path + str(sha_hash) + ".pdf"
    image_file_path = image_path + str(sha_hash) + ".png"
    results.append(file_path)
    try:
        h = requests.head(pdf.link, allow_redirects=True)
        header = h.headers
        content_type = header.get('content-type')
        r = requests.get(pdf.link, allow_redirects=True)
        print(content_type)

        if 'pdf' not in content_type:
            print("Sorry, content type not allowed " + content_type)
            return 'not pdf'
    except Exception as e:
        print(e)
        return 'error'

    with open(file_path, 'wb') as file:
        file.write(r.content)
    text = ''
    with fitz.open(file_path) as doc:
        page = doc.load_page(0)
        pix = page.get_pixmap()
        pix.save(image_file_path)
        for page in doc:
            text += page.get_text("xhtml", flags = fitz.TEXT_PRESERVE_LIGATURES+fitz.TEXT_PRESERVE_WHITESPACE+fitz.TEXT_MEDIABOX_CLIP)
    pdf.thumb = str(sha_hash) + ".png"
    pdf.pdf_name = str(sha_hash) + ".pdf"
    pdf.text = text
    pdf.save()
    return pdf.id
